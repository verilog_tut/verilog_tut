module tb_reg_file ();

   parameter data_size = 8;
   parameter init_value = 0;
   parameter cell_address = 2;

   reg clk, reset, write, read, reset_sig, write_sig, read_sig;
   reg [cell_address-1:0] address, address_sig;
   reg [8:0] 		  status_i, status_i_sig;
   wire [8:0] 		  cfg_o;
   reg [data_size-1:0] data_to_dut, data_to_dut_sig;
   wire [data_size-1:0] data;

   defparam reg_1.data_bus_sz = data_size;
   defparam reg_1.cell_address = cell_address;
   
   always #5 clk <= ~clk;
     
   always @(posedge clk)
     begin
	reset_sig <= reset;
	write_sig <= write;
	read_sig <= read;
	data_to_dut_sig <= data_to_dut;
	status_i_sig <= status_i;
	address_sig <= address;
     end

   assign data = (write_sig) ? data_to_dut_sig : 'bz;
   
   reg_file reg_1(.clock(clk),
		  .reset(reset_sig),
		  .write(write_sig),
		  .read(read_sig),
		  .data(data),
		  .address(address_sig),
		  .status_i(status_i_sig),
		  .cfg_o(cfg_o));

   task init;
      begin
	 clk = 0;
	 write = 0;
	 read = 0;
	 data_to_dut = init_value;
	 reset = 1;
	 address = 0;
	 status_i = 0;
	 #13 reset = 0;
      end
   endtask // init

   task read_t;
      input [cell_address-1:0] ad;
      begin
	 read = 1;
	 address = ad;
	 @(posedge clk)
	   read = 0;
      end
   endtask // read

   task write_t;
      input [data_size-1:0] d;
      input [cell_address-1:0] ad;
      begin
	 write = 1;
	 address = ad;
	 data_to_dut = d;
	 @(posedge clk)
	   write = 0;
      end
   endtask // write

   task randomize_status;
	 status_i = {$random} % 511;
   endtask // randomize_status
   

   initial
     begin
	integer i, j;
	init;
	for ( i = 0; i <= 2 ; i = i+1 )
	  for ( j = 0; j <= 3 ; j = j+1 )
	    begin
//	     #25 write_t(i,{$random} % 4);
//	     #25 randomize_status();
//	     #25 read_t({$random} % 4);
	       #25 write_t(i,j);
	       status_i <= i;
	       #25 read_t(j);
	    end
	#40 $finish;
     end

   initial
     begin
      $shm_open("TB","w");
      $shm_probe(tb_reg_file,"AC");
     end

endmodule // tb_reg_file
