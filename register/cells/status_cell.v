module status_cell ( clk, read, status_value,
		     data);

   parameter data_size = 8;

   reg [data_size-1:0] data_out;
   input wire  clk, read;
   output wire [data_size-1:0] data;
   input wire  [data_size-1:0] status_value;

   assign data = (read) ? data_out : 'bz;
   
   always @(posedge clk)
       data_out <= status_value;

endmodule // register
