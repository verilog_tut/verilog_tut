module cfg_cell ( clk, reset, write, read, 
		  data,
		  cfg_cell_q);

   parameter data_size = 8;

   input wire clk, reset, write, read;
   output wire [data_size-1:0] cfg_cell_q;
   inout wire [data_size-1:0] data;
   reg [data_size-1:0] 	data_out;
   wire [data_size-1:0] data_in;

   assign data = (read) ? data_out : 'bz;
   assign data_in = (write) ? data : data_out;
   assign cfg_cell_q = data_out;
   
   always @(posedge clk)
     if (reset)
       data_out <= 'b0;
     else
       data_out <= data_in;

endmodule // register
