module tb_reg_cell ();

   parameter data_size = 8;
   parameter init_value = 0;

   reg clk, reset, write, read, reset_sig, write_sig, read_sig;
   reg [data_size-1:0] data_to_dut, data_to_dut_sig;
   wire [data_size-1:0] data;

   defparam reg_1.data_size = data_size;
   
   always #5 clk <= ~clk;
     
   always @(posedge clk)
     begin
	reset_sig <= reset;
	write_sig <= write;
	read_sig <= read;
	data_to_dut_sig <= data_to_dut;
     end

   assign data = (write_sig) ? data_to_dut_sig : 'bz;
   
   reg_cell reg_1(.clk(clk),.reset(reset_sig),.write(write_sig),.read(read_sig),.data(data));

   task init;
      begin
	 clk = 0;
	 write = 0;
	 read = 0;
	 data_to_dut = init_value;
	 reset = 1;
	 #13 reset = 0;
      end
   endtask // init

   task read_t;
      begin
	 read = 1;
	 @(posedge clk)
	   read = 0;
      end
   endtask // read

   task write_t;
      input [data_size-1:0] d;
      begin
	 write = 1;
	 data_to_dut = d;
	 @(posedge clk)
	   write = 0;
      end
   endtask // write

   initial
     begin
	integer i;
	init;
	for ( i = 0 ; i <= 10 ; i = i+1 )
	  begin
	     #25 write_t(i);
	     #25 read_t;
	  end
	#40 $finish;
     end

   initial
     begin
      $shm_open("TB","w");
      $shm_probe(tb_reg_cell,"AC");
     end

endmodule // tb_register
