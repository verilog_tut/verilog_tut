module reg_file(clock, reset, read, write, // Interface for regfile
		data, address,
		status_i, cfg_o);  // Interface for cells

   parameter cell_address = 2;
   parameter data_bus_sz = 8;

   input wire clock, reset, read, write;
   input wire [cell_address-1:0]  address;
   inout wire [data_bus_sz-1:0]   data;
   input wire [8:0] status_i;
   output wire [8:0] cfg_o;
   
   reg [3:0] read_int;
   reg [1:0] write_int; // internal signals for read and write
   
   status_cell #(8) status_1(.clk(clock),
			     .data(data),
			     .status_value(status_i[8:1]),
			     .read(read_int[3]));
   status_cell #(1) status_2(.clk(clock),
			     .data(data[0]),
			     .status_value(status_i[0]),
			     .read(read_int[2]));
   cfg_cell #(8) cfg_1(.clk(clock),
		       .data(data),
		       .reset(reset),
		       .cfg_cell_q(cfg_o[8:1]),
		       .read(read_int[1]),
		       .write(write_int[1]));
   cfg_cell #(1) cfg_2(.clk(clock),
		       .data(data[0]),
		       .reset(reset),
		       .cfg_cell_q(cfg_o[0]),
		       .read(read_int[0]),
		       .write(write_int[0]));

   assign read_int[0] = (address == 0) ? read : 0 ;
   assign read_int[1] = (address == 1) ? read : 0 ;
   assign read_int[2] = (address == 2) ? read : 0 ;
   assign read_int[3] = (address == 3) ? read : 0 ;
   assign write_int[0] = (address == 0) ? write : 0 ;
   assign write_int[1] = (address == 1) ? write : 0 ;
    
endmodule // reg_file
