`define enable_s 1
`define disable_s 0
`define enable_addr 0
`define max_count_addr 1
`define count_addr 3
`define top_addr 2

module tb_reg_counter ();
   
   parameter data_size = 8;
   parameter init_value = 0;
   parameter cell_address = 2;

   reg clk, reset, write, read, reset_sig, write_sig, read_sig;
   reg [cell_address-1:0] address, address_sig;
   wire [8:0] status_i, cfg_o;
   reg [data_size-1:0] data_to_dut, data_to_dut_sig;
   wire [data_size-1:0] data;

   defparam reg_1.data_bus_sz = data_size;
   defparam reg_1.cell_address = cell_address;
   defparam count_1.counter_size = data_size;
   
   always #5 clk <= ~clk;
     
   always @(posedge clk)
     begin
	reset_sig <= reset;
	write_sig <= write;
	read_sig <= read;
	data_to_dut_sig <= data_to_dut;
	address_sig <= address;
     end

   assign data = (write_sig) ? data_to_dut_sig : 'bz;
   
   reg_file reg_1(.clock(clk),
		  .reset(reset_sig),
		  .write(write_sig),
		  .read(read_sig),
		  .data(data),
		  .address(address_sig),
		  .status_i(status_i),
		  .cfg_o(cfg_o));

   counter count_1(.enable(cfg_o[0]),
		   .clk(clk),
		   .rst(reset_sig),
		   .count(status_i[8:1]),
		   .top(status_i[0]),
		   .max_count(cfg_o[8:1]));

   task init;
      begin
	 clk = 0;
	 write = 0;
	 read = 0;
	 data_to_dut = init_value;
	 reset = 1;
	 address = 0;
	 disable_counter_t;
	 #13 reset = 0;
      end
   endtask // init

   task read_t;
      input [cell_address-1:0] ad;
      begin
	 read = 1;
	 address = ad;
	 @(posedge clk)
	   read = 0;
      end
   endtask // read

   task write_t;
      input [data_size-1:0] d;
      input [cell_address-1:0] ad;
      begin
	 write = 1;
	 address = ad;
	 data_to_dut = d;
	 @(posedge clk)
	   write = 0;
      end
   endtask // write

   task enable_counter_t;
      write_t(`enable_s,`enable_addr);
   endtask // enable_counter
   
   task	disable_counter_t;
      write_t(`disable_s,`enable_addr);
   endtask // disable_counter
   
   task set_max_count_t;
      input [data_size-1:0] maximum;
      write_t(maximum,`max_count_addr);
   endtask // set_max_count
   
   task get_count_status_t;
      read_t(`count_addr);
   endtask // get_count_status

   task get_top_status_t;
      read_t(`top_addr);
   endtask // get_top_status
     
   initial
     begin
	integer i, j;
	init;
	set_max_count_t(9);
	enable_counter_t;
	for (i = 0; i <= 15 ; i=i+1)
	  begin
	     #15 get_count_status_t;
	     #15 get_top_status_t;
	  end
	
/* -----\/----- EXCLUDED -----\/-----
	for ( i = 0; i <= 2 ; i = i+1 )
	  for ( j = 0; j <= 3 ; j = j+1 )
	    begin
//	     #25 write_t(i,{$random} % 4);
//	     #25 read_t({$random} % 4);
	       #25 write_t(i,j);
	       #25 read_t(j);
	    end
 -----/\----- EXCLUDED -----/\----- */
	#40 $finish;
     end

   initial
     begin
      $shm_open("TB","w");
      $shm_probe(tb_reg_counter,"AC");
     end

endmodule // tb_reg_counter
