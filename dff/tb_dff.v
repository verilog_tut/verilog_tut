module TB_DFF ( );

   wire Q;
   reg 	D, RST, EN, D_SIG, EN_SIG, RST_SIG, CLK;

   always @(posedge CLK)
     begin
	D_SIG <= D;
	EN_SIG <= EN;
	RST_SIG <= RST;
     end   

   always #5 CLK <= ~CLK;

   DFF DFF_1(.D(D_SIG),.CLK(CLK),.EN(EN_SIG),.RST(RST_SIG),.Q(Q));

   initial begin
      CLK = 0;
   end
   
   initial begin
      D = 0;
      #30 D = 1;
      #30 D = 0;
      #50 $finish;
   end

   initial begin
      EN = 0;
      #20 EN = 1;
   end

   initial begin
      RST = 1;
      #7 RST = 0;
   end

   initial begin
      $shm_open("TB","w");
      $shm_probe(TB_DFF,"AC");
   end

endmodule // TB_DFF
