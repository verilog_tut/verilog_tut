module counter ( enable, clk, rst, count, top, max_count );

   parameter counter_size = 4;
   
   input wire clk, rst, enable;
   input wire [counter_size-1:0] max_count;
   output reg [counter_size-1:0] count;
   output reg 			 top;

   always @(posedge clk)
     if (rst) // synchronous reset
       begin
	  count <= 0;
	  top <= 0;
       end
     else if (enable)
       if (count < max_count)
	 begin
	    count <= count +1;
	    top <= 0;
	 end
       else if (count == max_count)
	 begin
	    count <= 0;
	    top <= 1;
	 end
   
endmodule // counter
